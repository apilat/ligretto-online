const gid = id => document.getElementById(id);

Card.prototype.DISPLAY_COLOR = {
    RED: "#FF8E8E", GREEN: "#ABFF73", BLUE: "#9191FF", YELLOW: "#EEF093",
}
Card.prototype.elem = function (pos) {
    var elem = document.createElement("div");
    elem.className = "card";
    elem.style.background = this.DISPLAY_COLOR[this.color];
    elem.innerText = this.number + "";
    elem.dataset.repr = this.color + this.number;

    if (pos > 0) {
        elem.draggable = true;

        elem.addEventListener("dragstart", (e) => {
            e.dataTransfer.setData("frompos", pos);
        });
    }
    return elem;
}

function updateView() {
    if (running == PLAYING) {
        updatePlayerView();
    } else if (running == SPECTATING) {
        updateSpectatorView();
    }
    updateEnemiesView();
    updateBoardView();
    updateScoretableView();
    updateVotesView();
}

const HAND_POSITIONS = ["inhand", "stack", "row0", "row1", "row2"];

function updatePlayerView() {
    let player = game.getPlayer(currentPlayer);
    gid("cardcount").textContent = `(${player.stack.length}\u00a0/\u00a0${player.getCardCount()})`;

    for (var i = 1; i <= HAND_POSITIONS.length; i++) {
        var name = HAND_POSITIONS[i - 1];
        var el = gid(name);
        var card = player.getCard(i);

        if (el.firstChild) {
            el.removeChild(el.firstChild);
        }

        if (card) {
            el.appendChild(card.elem(i));
        } else if (name == "stack") {
            let stopElem = document.createElement("div");
            stopElem.className = "stop";
            stopElem.addEventListener("click", (_) => tryFinishRound());
            el.appendChild(stopElem);
        }
    }
}

function updateSpectatorView() {
    gid("cardcount").textContent = "SPECTATING";
}

function updateEnemiesView() {
    // TODO Don't redraw if nothing has changed
    var ems = gid("enemies");
    while (ems.firstChild)
        ems.removeChild(ems.firstChild);

    game.players.forEach(p => {
        if (p.id == currentPlayer) return;
        let player = game.getPlayer(p.id);

        var el = document.createElement("div");
        el.className = "enemy";

        var desc = document.createElement("span");
        desc.className = "enemyname";
        var name = document.createElement("span");
        name.innerText = player.name;
        name.style.color = player.getColor();
        name.style.padding = "4px";
        var stack = document.createElement("span");
        stack.innerText = `(${player.stack.length}\u00a0/\u00a0${player.getCardCount()})`;

        desc.appendChild(name);
        desc.appendChild(stack);
        el.appendChild(desc);

        for (var i = 1; i <= HAND_POSITIONS.length; i++) {
            var name = HAND_POSITIONS[i - 1];
            var card = player.getCard(i);
            if (card)
                el.appendChild(card.elem(0));
        }

        ems.appendChild(el);
    });
}

function updateBoardView() {
    for (var i = 0; i < 40; i++) {
        var el = gid(`table${i}`);
        var card = game.getPileTop(i);
        if (el.firstChild) {
            el.removeChild(el.firstChild);
        }
        if (card)
            el.appendChild(card.elem(0));
    }
}

function updateScoretableView() {
    var st = gid("scoreplayers");
    while (st.firstChild)
        st.removeChild(st.firstChild);

    game.players.forEach(p => {
        var el = document.createElement("li");
        el.className = "playerscore";

        var name = document.createElement("b");
        name.innerText = p.name;
        name.style.color = p.getColor();
        el.appendChild(name);

        for (var r = 0; r < game.numRounds(); r++) {
            var score = game.getScore(p.id, r);
            var score_display = document.createElement("span");
            if (typeof score === "number")
                score_display.innerText = score.toString();
            else
                score_display.innerText = "-";
            el.appendChild(score_display);
        }

        var total = document.createElement("b");
        total.innerText = game.getTotalScore(p.id);
        el.appendChild(total);

        st.appendChild(el);
    });
}

function updateVotesView() {
    const pln = game.players.length;
    const rstv = Object.keys(game.voteStatus.reset).length;
    const shufflev = Object.keys(game.voteStatus.shuffle).length;
    const startv = Object.keys(game.voteStatus.start).length;
    if (game.voteStatus.reset.indexOf(currentPlayer) != -1)
        gid("resettext").innerText = `(${rstv}) / ${pln}`;
    else
        gid("resettext").innerText = `${rstv} / ${pln}`;
    if (game.voteStatus.shuffle.indexOf(currentPlayer) != -1)
        gid("shuffletext").innerText = `(${shufflev}) / ${pln}`;
    else
        gid("shuffletext").innerText = `${shufflev} / ${pln}`;
    if (game.voteStatus.start.indexOf(currentPlayer) != -1)
        gid("starttext").innerText = `(${startv}) / ${pln}`;
    else
        gid("starttext").innerText = `${startv} / ${pln}`;
}

function tryMoveCard(frompos, topos) {
    socket.send(JSON.stringify({
        id: currentPlayer,
        token: token,
        command: "move_card",
        frompos: frompos,
        topos: topos,
    }));
}

function tryRetakeHand() {
    socket.send(JSON.stringify({
        id: currentPlayer,
        token: token,
        command: "retake_hand",
    }));
}

function tryFinishRound() {
    socket.send(JSON.stringify({
        id: currentPlayer,
        token: token,
        command: "finish_round",
    }));
}

function tryVote(vote) {
    socket.send(JSON.stringify({
        id: currentPlayer,
        token: token,
        command: "vote",
        vote: vote,
    }));
}

function setStatusText(text) {
    gid("statustext").innerText = text;
}

function showPlayCard(id, _frompos, topos) {
    if (id != currentPlayer) {
        gid(`table${topos}`).firstChild.classList.add("highlight-card");
        playSound(gid("audioenemy"));
    } else {
        playSound(gid("audioenemy"));
    }
}

var MOUSE_DELETE_TIMEOUT = 5000;
var mouseDeleteTimeouts = {};
function showMouseMove(id, pos, dragging) {
    if (id == currentPlayer) {
        return;
    }

    if (mouseDeleteTimeouts.hasOwnProperty(id)) {
        clearTimeout(mouseDeleteTimeouts[id]);
        delete mouseDeleteTimeouts[id];
    }

    const body = document.getElementsByTagName("body")[0];
    var elem = document.getElementById(`mouse_${id}`);

    if (!elem) {
        let player = game.getPlayer(id);
        elem = document.createElement("div");
        elem.id = `mouse_${id}`;
        elem.className = "enemy-mouse";
        elem.innerText = player.name;
        elem.style.color = player.getColor();
        body.appendChild(elem);
    }

    if (dragging) {
        elem.classList.add("enemy-mouse-dragging");
    } else {
        elem.classList.remove("enemy-mouse-dragging");
    }

    let [x, y] = [pos.x * window.innerWidth, pos.y * window.innerHeight];
    let tableRect = gid("table").getBoundingClientRect();
    if (tableRect.left <= x && x <= tableRect.right && tableRect.top <= y && y <= tableRect.bottom) {
        elem.style.opacity = "70%";
    } else {
        elem.style.opacity = "30%";
    }

    elem.style.left = `${x}px`;
    elem.style.top = `${y}px`;

    mouseDeleteTimeouts[id] = setTimeout(() => body.removeChild(elem), MOUSE_DELETE_TIMEOUT);
}

function playSound(audio) {
    if (gid("setaudio").checked) {
        audio.volume = 0.2;
        audio.pause();
        audio.currentTime = 0;
        audio.play();
    }
}


var game;
var token, currentPlayer, gameId;
const WAITING = 0, PLAYING = 1, SPECTATING = 2;
var running = WAITING;
var socket;

function startGame(remakeSocket) {
    gid("startgame").style.display = "none";
    gid("main").style.display = "";

    document.querySelectorAll(".tablecontainer").forEach(o => {
        o.addEventListener("drop", (e) => {
            var frompos = parseInt(e.dataTransfer.getData("frompos"));
            var id = e.target.id;
            if (!id)
                id = e.target.parentNode.id;
            var topos = parseInt(id.substring(5, id.length));

            tryMoveCard(frompos, topos);
        });
    });

    document.addEventListener("drop", e => e.preventDefault());
    document.addEventListener("dragover", e => e.preventDefault());
    document.addEventListener("selectstart", e => e.preventDefault());

    gid("hand").addEventListener("contextmenu", (e) => {
        // Right click
        tryRetakeHand();
        e.preventDefault();
    });

    gid("resetbutton").addEventListener("click", () => tryVote("reset"));
    gid("shufflebutton").addEventListener("click", () => tryVote("shuffle"));
    gid("startbutton").addEventListener("click", () => tryVote("start"));

    var MOUSE_UPDATE_INTERVAL = 50;
    var lastMousePosition, lastMouseUpdateTime = Date.now();
    const handleMouseMove = (evt, dragging) => {
        lastMousePosition = {"x": evt.clientX / window.innerWidth, "y": evt.clientY / window.innerHeight};
        var now = Date.now();
        if (now - lastMouseUpdateTime > MOUSE_UPDATE_INTERVAL) {
            lastMouseUpdateTime = now;
            if (socket && socket.readyState == WebSocket.OPEN && !window.disableMouseMove) {
                socket.send(JSON.stringify({
                    id: currentPlayer,
                    token: token,
                    command: "mouse_move",
                    pos: lastMousePosition,
                    dragging: dragging,
                }));
            }
        }
    };
    document.addEventListener("mousemove", evt => handleMouseMove(evt, false));
    document.addEventListener("dragover", evt => handleMouseMove(evt, true));

    gid("form").addEventListener("submit", evt => {
        evt.preventDefault();
        if (socket && socket.readyState == WebSocket.OPEN) {
            let text = gid("secret").value;
            socket.send(JSON.stringify({
                id: currentPlayer,
                token: token,
                command: "secret_command",
                code: text,
            }));
        }
    });

    if (remakeSocket) {
        gameId = gid("gamename").value;
        socket = new WebSocket(`ws://${window.location.host}/socket/${gameId}`);
        socket.onopen = () => {
            setStatusText("Successfully connected to server");
            socket.send(JSON.stringify({
                command: "request_player",
                name: gid("username").value,
                spectate: gid("spectate").checked,
            }));
        };
        socket.onmessage = handleSocketMessage;
        socket.onclose = () => {
            setStatusText("Connection closed unexpectedly");
        };
        socket.onerror = (err) => {
            setStatusText(`Connection terminated by error: ${err.data}`);
        };
    }
}

function handleSocketMessage(ev) {
    var data = JSON.parse(ev.data);
    if (window.debug) {
        console.log(data);
    }
    if (running == WAITING) {
        if (data.command == "give_token") {
            token = data.token;
            currentPlayer = data.id;
            running = PLAYING;
            localStorage.setItem("previousGameDatav1", JSON.stringify({
                game: gameId,
                token: token,
            }));
        } else if (data.command == "allow_spectate") {
            currentPlayer = null;
            running = SPECTATING;
        }
        return;
    }

    // break - redraw, return - don't redraw
    switch (data.command) {
        case "sync_game":
            game = Ligretto.decode(data.data);
            gid("startoverlay").style.display = game.playing ? "none" : "block";
            break;
        case "set_overlay":
            if (data.enabled) {
                gid("startoverlay").style.display = "block";
                gid("startoverlay").textContent = data.content;
            } else {
                gid("startoverlay").style.display = "none";
            }
            return;
        case "move_made":
            showPlayCard(data.id, data.frompos, data.topos);
            return;
        case "mouse_move":
            showMouseMove(data.id, data.pos, data.dragging);
            return;
        case "execute_cmd":
            game.executeCommand(data.cmd);
            break;
        default:
            setStatusText(`Unexpected command ${data.command}`);
            break;
    }
    updateView();
}

function tryToReconnect() {
    let prev = localStorage.getItem("previousGameDatav1");
    if (prev === undefined) return;
    prev = JSON.parse(prev);

    socket = new WebSocket(`ws://${window.location.host}/socket/${prev.game}`);
    socket.onopen = () => {
        setStatusText("Successfully connected to server");
        socket.send(JSON.stringify({
            command: "try_rejoin",
            token: prev.token
        }));
    };
    socket.onmessage = (ev) => {
        const data = JSON.parse(ev.data);
        if (data.command === "rejoin_valid") {
            gid("rejoin").style.display = "block";
            gid("rejoingamename").textContent = prev.game;
            gid("rejoinplayername").textContent = data.name;
            gid("rejoinsubmit").addEventListener("click", () => {
                gameId = prev.game;
                socket.onmessage = handleSocketMessage;
                socket.send(JSON.stringify({
                    command: "rejoin_confirm"
                }));
                startGame(false);
            });
        } else {
            assert(data.command == "rejoin_bad");
            socket.close();
        }
    };
}

document.addEventListener("DOMContentLoaded", tryToReconnect);
