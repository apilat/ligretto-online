const express = require('express');
const app = express();
const expressWs = require('express-ws')(app);

const crypto = require('crypto');
const util = require('util');

const ligretto = require('./shared/ligretto.js');
const secret = process.env.SECRET || "1337_hax0r";
const debugEnabled = process.argv.length >= 3 && process.argv[2] == "debug";

function send(ws, msg) {
    if (ws.readyState == 1)
        ws.send(msg);
}

class Game {
    constructor() {
        this.players = [];
        this.lockedInPlayers = null;
        this.baseid = 1;
        this.lig = new ligretto.Ligretto();
        this.setupGame();
    }

    setupGame() {
        this.lig.setCallback("start", () => {
            if (this.lockedInPlayers === null) {
                this.lockedInPlayers = this.players.filter(x => x.isPlaying());
            }

            this.broadcast(JSON.stringify({
                command: "set_overlay", enabled: false
            }))

            if (debugEnabled) {
                for (const [c, n] of Object.entries(ligretto.Card.prototype.COLOR)) {
                    for (let i = 1; i <= 9; i++) {
                        this.lig.piles[n * 10 + i] = new ligretto.Card(c, i);
                    }
                }
                this.broadcast(this.syncmsg());
            }
        });
        this.lig.setCallback("finish", () => this.broadcast(JSON.stringify({
            command: "set_overlay", enabled: true, content: ""
        })));
        this.lig.setCallback("countdown", (t) => this.broadcast(JSON.stringify({
            command: "set_overlay", enabled: true, content: t.toString(),
        })));
        this.lig.setCallback("reset", () => this.broadcast(JSON.stringify({
            command: "set_overlay", enabled: true
        })));
    }

    isLocked() {
        return this.lockedInPlayers !== null;
    }

    isEmpty() {
        return this.players.length === 0;
    }

    getPlayer(id) {
        var p = this.players.filter(p => p.id == id);
        if (p.length == 1)
            return p[0];
        else
            return null;
    }

    addPlayer(name, seed, token, ws) {
        const id = this.baseid++;
        const lp = new ligretto.Player(id, name, seed, this.lig.findColorIdx());
        const gp = new GamePlayer(id, token, ws, lp);
        this.lig.players.push(lp);
        this.players.push(gp);
        return id;
    }

    addSpectator(ws) {
        const id = this.baseid++;
        const gp = new GamePlayer(id, "", ws, null);
        this.players.push(gp);
        return id;
    }

    rejoinPlayer(player, ws) {
        player.ws = ws;
        this.lig.players.push(player.obj);
        this.players.push(player);
    }

    removePlayer(id) {
        this.players = this.players.filter(p => p.id != id);
        this.lig.players = this.lig.players.filter(p => p.id != id);
    }

    broadcast(msg) {
        this.players.forEach(p => {
            send(p.ws, msg);
        });
    }

    syncmsg() {
        return JSON.stringify({
            command: "sync_game",
            data: this.lig.encode(),
        });
    }

    findInactivePlayer(token) {
        if (!this.lockedInPlayers)
            return null;
        const lockedIn = this.lockedInPlayers.find(p => p.token == token);
        const current = this.players.find(p => p.token == token);
        if (lockedIn && !current) {
            return lockedIn;
        } else {
            return null;
        }
    }
}

class GamePlayer {
    constructor(id, token, ws, obj) {
        this.id = id;
        this.token = token;
        this.ws = ws;
        this.obj = obj;
    }

    isPlaying() {
        return this.obj !== null;
    }
}

var runningGames = new Map();
app.use('/', express.static('web/'));
app.use('/', express.static('shared/'));

app.get('/status/:game', function (req, res) {
    const gameId = req.params.game;
    const game = runningGames.get(gameId);
    if (game) {
        res.send(JSON.stringify({running: true, locked: game.isLocked()}));
    } else {
        res.send(JSON.stringify({running: false}));
    }
});

app.ws('/socket/:game', function (ws, req) {
    const gameId = req.params.game;
    var game;
    if (runningGames.has(gameId)) {
        game = runningGames.get(gameId);
    } else {
        game = new Game();
        runningGames.set(gameId, game);
    }
    var pid, token;
    var inGame = false, rejoinPlayer = null;

    ws.on('message', function (msg) {
        var data = JSON.parse(msg);
        if (!inGame) {
            if (data.command === "request_player") {
                if (data.spectate || game.isLocked()) {
                    pid = game.addSpectator(ws);

                    send(ws, JSON.stringify({
                        command: "allow_spectate"
                    }));
                    send(ws, game.syncmsg());

                } else {
                    const seed = Math.floor(Math.random() * (1 << 30));
                    token = crypto.randomBytes(48).toString("hex");
                    pid = game.addPlayer(data.name, seed, token, ws);

                    send(ws, JSON.stringify({
                        command: "give_token",
                        token: token,
                        id: pid,
                    }));
                    game.broadcast(game.syncmsg());
                }
                inGame = true;
                return;
            } else if (data.command === "try_rejoin") {
                let player = game.findInactivePlayer(data.token);
                if (player) {
                    send(ws, JSON.stringify({
                        command: "rejoin_valid",
                        name: player.obj.name,
                    }));
                    rejoinPlayer = player;
                } else {
                    send(ws, JSON.stringify({command: "rejoin_bad"}));
                }
            } else if (data.command == "rejoin_confirm") {
                if (rejoinPlayer === null) {
                    ws.close();
                    return;
                }
                token = rejoinPlayer.token;
                pid = rejoinPlayer.id;
                game.rejoinPlayer(rejoinPlayer, ws);
                send(ws, JSON.stringify({
                    command: "give_token",
                    token: token,
                    id: pid,
                }));
                game.broadcast(game.syncmsg());
                inGame = true;
                return;
            } else {
                ws.close();
                return;
            }
        }

        if (!data.command || !data.id || !data.token) {
            return;
        }
        var id = data.id;
        if (!game.getPlayer(id) || game.getPlayer(id).token != data.token) {
            return;
        }
        delete data.token;

        try {
            switch (data.command) {
                case "move_card":
                    // Should do more handling to make sure that game does not assert but for now
                    // assume that agents are not malicious.
                    if (!data.hasOwnProperty("frompos") || !data.hasOwnProperty("topos"))
                        break;
                    if (game.lig.playCard(id, data.frompos, data.topos)) {
                        game.broadcast(game.syncmsg());
                        game.broadcast(JSON.stringify({
                            command: "move_made",
                            id: id,
                            frompos: data.frompos,
                            topos: data.topos,
                        }));
                    } else {
                        send(ws, game.syncmsg());
                    }
                    break;
                case "retake_hand":
                    if (game.lig.retakeHand(id))
                        game.broadcast(game.syncmsg());
                    break;
                case "finish_round":
                    if (game.lig.getPlayer(id).stack.length == 0) {
                        game.lig.finishRound();
                        game.broadcast(game.syncmsg());
                    }
                    break;
                case "vote":
                    if (!data.hasOwnProperty("vote") || !["reset", "shuffle", "start"].includes(data.vote))
                        break;
                    game.lig.toggleVote(data.vote, pid);
                    game.broadcast(game.syncmsg());
                    break;
                case "sync_game":
                    send(ws, game.syncmsg());
                    break;
                case "mouse_move":
                    game.broadcast(JSON.stringify(data));
                    break;
                case "secret_command":
                    let tmp = data.code.split(" ");
                    if (tmp[0] == secret) {
                        game.lig.executeCommand(tmp[1]);
                        game.broadcast(JSON.stringify({
                            command: "execute_cmd",
                            cmd: tmp[1],
                        }));
                    }
                    break;
                default:
                    console.warn(`unknown command ${data.command}`);
            }
        } catch (e) {
            console.error(`error occured: ${e.stack}`);
        }
    });

    ws.on('close', function () {
        game.removePlayer(pid);
        game.broadcast(game.syncmsg());

        if (game.isEmpty()) {
            runningGames.delete(gameId);
        }
    });
});

app.listen(8080);
