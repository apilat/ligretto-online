"use strict";

function assert(cond, mess) {
    if (!cond) {
        throw 'Assertion failed: ' + mess;
    }
}

// PRNG
function mulberry32(obj) {
    return function () {
        var t = obj.seed += 0x6D2B79F5;
        t = Math.imul(t ^ t >>> 15, t | 1);
        t ^= t + Math.imul(t ^ t >>> 7, t | 61);
        return ((t ^ t >>> 14) >>> 0) / 4294967296;
    }
}

class Card {
    constructor(color, number) {
        assert(this.COLOR.hasOwnProperty(color), `Invalid card color ${color}`);
        assert(1 <= number && number <= 10, `Invalid card number ${number}`);
        this.color = color;
        this.number = number;
    }

    encode() {
        return this.COLOR[this.color] * 11 + this.number;
    }

    static decode(x) {
        return new Card(Card.prototype.COLOR_LIST[Math.floor(x / 11)], x % 11);
    }
}
Card.prototype.COLOR = {
    RED: 0, GREEN: 1, BLUE: 2, YELLOW: 3
}
Card.prototype.COLOR_LIST = ["RED", "GREEN", "BLUE", "YELLOW"]

function genDeck(player) {
    var pile = [];
    for (const c of Card.prototype.COLOR_LIST) {
        for (var i = 1; i <= 10; i++) {
            pile.push(new Card(c, i));
        }
    }
    var j, i;
    for (i = pile.length - 1; i > 0; i--) {
        j = Math.floor(player.random() * (i + 1));
        [pile[i], pile[j]] = [pile[j], pile[i]];
    }
    return pile;
}

class Player {
    constructor(id, name, seed, colorIdx) {
        this.id = id;
        this.colorIdx = colorIdx;
        this.name = name;
        this.seed = seed;
        this.random = mulberry32(this);
        this.resetState();
    }

    encode() {
        return JSON.stringify({
            a: this.id,
            b: this.name,
            c: this.seed,
            d: this.curScore,
            e: this.stack.map(x => x ? x.encode() : x),
            f: this.row.map(x => x ? x.encode() : x),
            g: this.hand.map(x => x ? x.encode() : x),
            h: this.colorIdx,
        });
    }

    static decode(s) {
        s = JSON.parse(s);
        let obj = new Player(s.a, s.b, 0, s.h);
        obj.seed = s.c;
        obj.random = mulberry32(obj);
        obj.curScore = s.d;
        obj.stack = s.e.map(x => x ? Card.decode(x) : x);
        obj.row = s.f.map(x => x ? Card.decode(x) : x);
        obj.hand = s.g.map(x => x ? Card.decode(x) : x);
        return obj;
    }

    getCardCount() {
        return this.stack.length + this.hand.length + this.row.length;
    }

    resetState() {
        this.generateCards();
        this.curScore = 0;
    }

    generateCards() {
        var deck = genDeck(this);
        this.stack = deck.slice(0, 10);
        this.row = deck.slice(10, 13);
        this.hand = deck.slice(13, 40);
    }

    shuffleDeck() {
        var j, i;
        for (i = this.hand.length - 1; i > 0; i--) {
            j = Math.floor(this.random() * (i + 1));
            [this.hand[i], this.hand[j]] = [this.hand[j], this.hand[i]];
        }
    }

    getCard(pos) {
        assert(1 <= pos && pos <= 5, `Invalid position ${pos} in getCard`);
        switch (pos) {
            case this.POSITION.STACK:
                if (this.stack.length == 0)
                    return null;
                return this.stack[this.stack.length - 1];
            case this.POSITION.ROW0:
                return this.row[0];
            case this.POSITION.ROW1:
                return this.row[1];
            case this.POSITION.ROW2:
                return this.row[2];
            case this.POSITION.HAND:
                return this.hand[this.hand.length - 1];
        }
    }

    processCardPlay(pos) {
        assert(1 <= pos && pos <= 5, `Invalid position ${pos} in processCardPlay`);
        if (pos == this.POSITION.STACK) {
            this.stack.splice(this.stack.length - 1, 1);
        } else if (pos == this.POSITION.HAND) {
            this.hand.splice(this.hand.length - 1, 1);
        } else {
            var rowpos = pos - 3;
            if (this.stack.length > 0) {
                this.row[rowpos] = this.stack[this.stack.length - 1];
                this.stack.splice(this.stack.length - 1, 1);
            } else {
                this.row[rowpos] = null;
            }
        }
    }

    getScore() {
        return this.curScore - 2 * this.stack.length;
    }

    getColor() {
        return this.COLOR[this.colorIdx % this.COLOR.length];
    }
}
Player.prototype.POSITION = {
    HAND: 1, STACK: 2, ROW0: 3, ROW1: 4, ROW2: 5
}
Player.prototype.COLOR = [
    "#007df9", "#b10dc9", "#ff4136", "#2ecc40", "#39cccc", "#ffdc00", "#01ff70", "#f012be"
]

class Ligretto {
    constructor() {
        this.playing = false;
        this.players = [];
        this.piles = new Array(40);
        this.resetVoteStatus();
        this.resetCallbacks();
        this.resetGame();
    }

    resetGame() {
        this.playing = false;
        this.cleanBoard();
        this.rounds = [];
        if (this.callbacks.reset)
            this.callbacks.reset()
    }

    resetCallbacks() {
        this.callbacks = {start: null, finish: null, countdown: null, reset: null};
    }

    resetVoteStatus() {
        this.voteStatus = {reset: [], shuffle: [], start: []};
        this.voteActions = {
            reset: () => this.resetGame(),
            shuffle: () => this.forceShuffle(),
            start: () => this.startRoundCountdown(5),
        };
    }

    encode() {
        return JSON.stringify({
            a: this.playing,
            b: this.players.map(x => x.encode()),
            c: this.piles.map(x => x ? x.encode() : x),
            d: this.rounds,
            e: this.voteStatus
        });
    }

    static decode(s) {
        s = JSON.parse(s);
        let obj = new Ligretto();
        obj.playing = s.a;
        obj.players = s.b.map(x => Player.decode(x));
        obj.piles = s.c.map(x => x ? Card.decode(x) : x);
        obj.rounds = s.d;
        obj.voteStatus = s.e;
        return obj;
    }

    toggleVote(vote, p) {
        var index = this.voteStatus[vote].indexOf(p);
        if (index != -1) {
            this.voteStatus[vote].splice(index, 1); // remove
        } else {
            this.voteStatus[vote].push(p);
            if (this.voteStatus[vote].length == this.players.length) {
                this.voteActions[vote]();
                this.voteStatus[vote] = [];
                return true;
            }
        }
        return false;
    }

    setCallback(name, callback) {
        this.callbacks[name] = callback;
    }

    forceShuffle() {
        this.players.forEach(p => p.shuffleDeck());
    }

    addPlayer(p) {
        this.players.push(p);
    }

    removePlayer(id) {
        this.players = this.players.filter(p => p.id !== id);
    }

    getPlayer(id) {
        var p = this.players.filter(p => p.id === id);
        assert(p.length == 1, `Unique player ${id} not found`);
        return p[0];
    }

    getPileTop(pilen) {
        assert(0 <= pilen <= 40, `Invalid pilen ${pilen} in getPileTop`);
        return this.piles[pilen];
    }

    playCard(id, frompos, topos) {
        assert(0 <= topos <= 40, `Invalid topos ${topos} in playCard`);
        assert(1 <= frompos <= 4, `Invalid frompos ${frompos} in playCard`);
        if (!this.playing)
            return false;

        var pile = this.piles[topos];
        var player = this.getPlayer(id);
        var card = player.getCard(frompos);
        if ((pile == null && card.number == 1) || (pile != null && pile.number == card.number - 1 && pile.color == card.color)) {
            this.piles[topos] = card;

            player.curScore += 1;
            player.processCardPlay(frompos);

            this.resetVoteStatus();
            return true;
        }
        return false;
    }

    retakeHand(id) {
        if (!this.playing)
            return false;

        var p = this.getPlayer(id);
        p.hand = p.hand.slice(p.hand.length - 3, p.hand.length)
            .concat(p.hand.slice(0, p.hand.length - 3));
        return true;
    }

    cleanBoard() {
        for (var i = 0; i < 40; i++)
            this.piles[i] = null;
    }

    startRoundCountdown(timeout) {
        if (timeout == 0) {
            this.startRound();
        } else {
            if (this.callbacks.countdown)
                this.callbacks.countdown(timeout);
            setTimeout(() => this.startRoundCountdown(timeout - 1), 1000);
        }
    }

    startRound() {
        this.cleanBoard();
        this.playing = true;
        if (this.callbacks.start)
            this.callbacks.start()
    }

    finishRound() {
        var scores = {};
        for (const player of this.players) {
            scores[player.id] = player.getScore();
        }
        this.rounds.push(scores);
        this.players.forEach(p => p.resetState());
        this.playing = false;
        if (this.callbacks.finish)
            this.callbacks.finish();
    }

    getScore(id, round) {
        assert(0 <= round && round < this.rounds.length, `Invalid round ${round} in getScore`);
        var s = this.rounds[round][id];
        if (typeof s === "number")
            return s;
        return null;
    }

    getTotalScore(id) {
        var sum = 0;
        for (var i = 0; i < this.rounds.length; i++) {
            if (this.rounds[i][id])
                sum += this.rounds[i][id];
        }
        return sum;
    }

    numRounds() {
        return this.rounds.length;
    }

    executeCommand(cmd) {
        switch (cmd) {
            case "bad_round":
                var scores = {};
                for (const player of this.players) {
                    scores[player.id] = -this.getTotalScore(player.id);
                }
                this.rounds.push(scores);
                break;
        }
    }

    findColorIdx() {
        const cnt = Player.prototype.COLOR.length;
        let counts = new Array(cnt);
        for (let i = 0; i < cnt; i++) {
            counts[i] = 0;
        }
        for (let p of this.players) {
            counts[p.colorIdx] += 1;
        }
        return counts.indexOf(Math.min(...counts));
    }
}

module.exports.Card = Card
module.exports.Player = Player
module.exports.Ligretto = Ligretto
